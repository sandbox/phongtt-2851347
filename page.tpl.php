<!-- #header -->
<div class="page-header">
	<?php if ($site_name): ?>
	<h1><a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>
	<?php endif; ?>
	<p>
	<?php
		$date = new DateTime();
		echo $date->format('l F jS');
	?>
	</p>
</div><!-- EOF: #header -->
<nav class="navbar navbar-default navbar-transparent">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed pull-left" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
	</div>
	<div id="navbar" class="collapse navbar-collapse">
		<?php 
			if (module_exists('i18n_menu')) {
				$main_menu_tree = i18n_menu_translated_tree(variable_get('menu_main_links_source', 'main-menu'));
			} else {
				$main_menu_tree = menu_tree(variable_get('menu_main_links_source', 'main-menu')); 
			}
			print drupal_render($main_menu_tree);
		?>
	</div><!--/.nav-collapse -->
</nav>
<?php if ($breadcrumb): print $breadcrumb; endif;?>
<div class="col-xs-12 col-md-6">
	<?php print $messages; ?>
	<?php print render($title_prefix); ?>
	<?php if ($title): ?><h1 class="page-title"><?php print $title; ?></h1><?php endif; ?>
	<?php print render($title_suffix); ?>
	<?php if (!empty($tabs['#primary'])): ?><div class="tabs-wrapper"><?php print render($tabs); ?></div><?php endif; ?>
	<?php print render($page['help']); ?>
	<?php if ($action_links): ?><ul class="action-links"><?php print render($action_links); ?></ul><?php endif; ?>
	<?php print render($page['content']); ?>
</div>
<div class="col-xs-6 col-md-6">
	<div class="col-md-8">
		<div class="col-md-12">Top</div>
		<div class="col-md-6">First</div>
		<div class="col-md-6">Second</div>
	</div>
	<div class="col-md-4">Third</div>
</div>
